describe('Todo', () => {
    it('did be input focus ?', () => {
        cy.visit('http://localhost:3000/');

        cy.focused()
        .get('#write-todo')
        .should('have.focus');
    });

    context('Add Todo', () => {
        const todo = 'test-task';
    
        it('Add Todo', () => {
            cy.get('#write-todo')
            .type(todo)
            .should('have.value', todo);
        });

        it('Has todo been added?', () => {
            cy.get('#add-btn')
            .click();
        });
    });
});