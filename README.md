## Run

### `yarn start`
Project run on [http://localhost:3000] (default)

## Cypress Tests Run

### `yarn run cypress`

## Run Project at Docker
docker-compose up

## Creating a new Task
  <a>
    <img src="post.png" alt="Logo" >
  </a>


## Listing Tasks


<a>
    <img src="get.png" alt="Logo" >
  </a>


## Test

<a>
    <img src="test.png" alt="Logo" >
  </a>
