
import React, { useEffect, useState } from "react";
import './App.css';
import axios from "axios";

const App = (props) => {

  const [todo, setTodo] = useState([]);
  const [newTodo, setNewTodo] = useState();
  const [load, setLoad] = useState(false);
  const [saveButton] = useState(false);


  useEffect(() => {
    axios.get('/todos')
      .then((response) => {
        setTodo(response.data)
      });

  }, []);

  const todoChange = (event) => {
    if (event.target.value !== "") {
      setLoad(true);
      setNewTodo(event.target.value);
    } else if (event.target.value === "") {
      setLoad(false);
    }

  }
  const addTodo = () => {
    if (load === false) {
    } else if (load === true) {

      const body = {
        title: newTodo,
      }

      axios.post('/todos', body)
        .then((response) => {
          todo.push(response.data);
          setTodo([...todo], response.data);
        })
    }
  }

  return (
    <div className="App" id="app">
      <h2>
        <span>TODO</span>
        <input autoFocus id="write-todo" placeholder="Write todo" onChange={todoChange}/>
        <button id="add-btn" onClick={addTodo}>+</button>
      </h2>
      {todo.map((todo, index) => (
        (
          <div style={{ marginBottom: "15px", background: "", padding: "10px", borderRadius: "10px" }}>
            <input className={todo.completed === false ? "changeInp" : "changeInpTrue"} defaultValue={todo.title} />
            {
              saveButton &&
              <button id="save-button"  style={{ float: "right", width: "50px", height: "40px", background: "#388e3c", fontSize: "12px", marginTop: "6px" }}></button>
            }
          </div>
        )
      ))}
    </div>
  );
}

export default App;
